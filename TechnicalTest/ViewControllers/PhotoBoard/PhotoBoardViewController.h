//
//  ViewController.h
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoBoardViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIAlertViewDelegate>


@end

