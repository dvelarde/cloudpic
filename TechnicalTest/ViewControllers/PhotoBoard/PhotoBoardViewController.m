//
//  ViewController.m
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "PhotoBoardViewController.h"
#import "DataAccessRep.h"
#import <AFNetworking/AFNetworking.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "NetworkingRep.h"
#import "PicCloudPhotoCollectionViewCell.h"
#import "FileManager.h"

@interface PhotoBoardViewController ()

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSArray *arrayPhotos;
@property (strong, nonatomic) NSNumber *numberOfItemsUpdated;
@property (nonatomic) BOOL queueCancelled;
@end

@implementation PhotoBoardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshAfterDelete) name:@"RefreshAfterDelete" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getData
{
    [SVProgressHUD showWithStatus:@"Let me get all your data. It will be just a minute :-)" maskType:SVProgressHUDMaskTypeBlack];
    
    AFHTTPRequestOperationManager *objManager = [NetworkingRep getCustomizedManager];
    
    [objManager GET:@"https://api.parse.com/1/classes/Gallery" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *results = [responseObject objectForKey:@"results"];
        
        for(NSDictionary *objPhoto in results)
        {
            [DataAccessRep createPhotoFromDictionary:objPhoto];
        }
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"updateDate"];
        [SVProgressHUD dismiss];
        self.arrayPhotos = [DataAccessRep retrievePhotos];
        [self.collectionView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"We tried hard but we can't get the data back. Would you like to try it again?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"I'll give it a try", nil];
        [alert show];
    }];
}

-(void)updateData
{

    [SVProgressHUD showWithStatus:@"Let me update your data. It will be just a minute :-)" maskType:SVProgressHUDMaskTypeBlack];
    
    AFHTTPRequestOperationManager *objManager = [NetworkingRep getCustomizedManager];
    
    [objManager GET:@"https://api.parse.com/1/classes/Gallery" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *results = [responseObject objectForKey:@"results"];
        
        for(NSDictionary *objPhoto in results)
        {
            if([DataAccessRep hasThisPhotoChangedOvertime:objPhoto])
            {
                if(![DataAccessRep updatePhoto:objPhoto])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"We tried hard but we can't get the data back. Would you like to try it again?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"I'll give it a try", nil];
                    
                    alert.tag = 2;
                    
                    [alert show];
                }
            }
        }
        
        NSArray *dataToUpdate = [DataAccessRep getDataToUpload];
        if(dataToUpdate == nil || [dataToUpdate count] == 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"updateDate"];
            [SVProgressHUD dismiss];
            
            self.arrayPhotos = [DataAccessRep retrievePhotos];
            
            [self.collectionView reloadData];
        }
        else{
            self.numberOfItemsUpdated = [NSNumber numberWithInt:0];
            
            for(NSDictionary *objPhoto in dataToUpdate){
                
                [objManager POST:@"https://api.parse.com/1/classes/Gallery" parameters:objPhoto success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    [DataAccessRep updateDatabaseWithRegisteredData:responseObject ForUrl:[objPhoto objectForKey:@"pictureUrl"]];
                    
                    self.numberOfItemsUpdated = [NSNumber numberWithInt:([self.numberOfItemsUpdated intValue] + 1)];
                    
                    if([dataToUpdate count] == [self.numberOfItemsUpdated intValue])
                    {
                        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"updateDate"];
                        [SVProgressHUD dismiss];
                        
                        self.arrayPhotos = [DataAccessRep retrievePhotos];
                        
                        [self.collectionView reloadData];
                    }
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"We tried hard but we can't get the data up. Would you like to try it again?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"I'll give it a try", nil];
                    alert.tag = 2;
                    [alert show];
                }];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"We tried hard but we can't get the data back. Would you like to try it again?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"I'll give it a try", nil];
        alert.tag = 2;
        [alert show];
    }];
}

-(void)badRequestForPhoto:(Photo *)objPhoto
{
    self.queueCancelled = YES;
    NSDictionary *objPhotoDict = [DataAccessRep dictionaryFromPhoto:objPhoto];
    if(objPhotoDict != nil){
        [[NSUserDefaults standardUserDefaults] setObject:objPhotoDict forKey:@"Photo"];
    
        [[[AFHTTPRequestOperationManager manager] operationQueue] cancelAllOperations];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"It looks like there is a problem trying to download some images. Please update the URL if you are missing something." delegate:self cancelButtonTitle:@"Oh Ok" otherButtonTitles:nil];
        alert.tag = 4;
    
        [alert show];
    }
}


-(void)viewDidAppear:(BOOL)animated
{
    self.queueCancelled = NO;
    
    User * objUser = [DataAccessRep retrieveUser];
    
    if(objUser == nil)
    {
        [self performSegueWithIdentifier:@"Welcome" sender:nil];
    }
    else
    {
        self.arrayPhotos = [DataAccessRep retrievePhotos];
        
        if(self.arrayPhotos == nil || [self.arrayPhotos count] == 0)
        {
            [self getData];
        }
        else
        {
            NSDate *updateDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"updateDate"];
            NSDate *nowDate = [NSDate date];
            
            if(updateDate == nil)
            {
                [self getData];
            }
            else
            {
                int diffTime = [nowDate timeIntervalSinceDate:updateDate];
                if(diffTime > 86400)
                {
                    //More than a day without update
                    [self updateData];
                    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"updateDate"];
                }
                else
                {
                    [self.collectionView reloadData];
                }
            }
                
        }
    }
}

#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [self.arrayPhotos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PicCloudPhotoCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    Photo *objPhoto = [self.arrayPhotos objectAtIndex:indexPath.item];
    
    if([FileManager imageExistsForID:objPhoto.objectId] && [objPhoto.ableToUpdate boolValue] == YES)
    {
        [cell.imgPhoto setImage:[FileManager getImageForID:objPhoto.objectId]];
        [cell.activityIndicator setHidden:YES];
    }
    else{
        if(objPhoto.url == nil)
        {
            [self badRequestForPhoto:objPhoto];
        }
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:objPhoto.url]];
        
        [cell.imgPhoto setImageWithURLRequest:request placeholderImage:[UIImage imageNamed:@"placeholderImage"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            [FileManager saveImage:image forID:objPhoto.objectId];
            [cell.imgPhoto setImage:image];
            [cell.imgPhoto setNeedsDisplay];
            [cell.activityIndicator setHidden:YES];
            
            if([objPhoto.ableToUpdate boolValue] == NO)
            {
                NSMutableDictionary *objPhotoDict = [[DataAccessRep dictionaryFromPhoto:objPhoto] mutableCopy];
                [objPhotoDict setObject:[NSNumber numberWithBool:YES] forKey:@"ableToUpdate"];
                [objPhotoDict setObject:[NSNumber numberWithBool:NO] forKey:@"uploaded"];
                
                [DataAccessRep updatePhoto:objPhotoDict];
                
                NSMutableDictionary *objPhotoUpload = [NSMutableDictionary new];
                [objPhotoUpload setObject:objPhoto.name forKey:@"name"];
                [objPhotoUpload setObject:objPhoto.desc forKey:@"description"];
                [objPhotoUpload setObject:objPhoto.url forKey:@"pictureUrl"];
                
                AFHTTPRequestOperationManager *objManager = [NetworkingRep getCustomizedManager];
                
                [objManager POST:@"https://api.parse.com/1/classes/Gallery" parameters:objPhotoUpload success:^(AFHTTPRequestOperation *operation, id responseObject) {
                    
                    [DataAccessRep updateDatabaseWithRegisteredData:responseObject ForUrl:objPhoto.url];
                    
                    
                } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    
                }];

                
            }
            
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            if(!self.queueCancelled)
                [self badRequestForPhoto:objPhoto];
            
            
        }];
    }
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize retval = CGSizeMake(96,96);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(6, 6, 6, 6);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Photo *objPhoto = [self.arrayPhotos objectAtIndex:indexPath.item];
    if(objPhoto.objectId == nil || [objPhoto.objectId isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Excuse me" message:@"You can not view photo details until the photo has been uploded to our system. Please refresh the data with the button on the up-right corner in order to upload the photo" delegate:self cancelButtonTitle:@"Oh Ok" otherButtonTitles:@"I want to", nil];

        [alert show];
    }
    else
    {
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"Photo"] == nil){
            [[NSUserDefaults standardUserDefaults] setObject:[objPhoto objectId] forKey:@"Photo"];
            [self performSegueWithIdentifier:@"ViewPhoto" sender:nil];
        }
    }
}
#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 2 || alertView.tag == 3)
    {
        if(buttonIndex == 1)
        {
            [self updateData];
        }
    }
    else
    {
        if(alertView.tag == 4)
        {
            [self performSegueWithIdentifier:@"PhotoEdit" sender:nil];
        }
        else
        {
            if(buttonIndex == 1)
            {
                [self getData];
            }
        }
    }
}

- (IBAction)touchedRefresh:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Excuse me" message:@"We try to keep all your data synced at least once a day. Still, if you want to reload it just let us know." delegate:self cancelButtonTitle:@"Oh Ok" otherButtonTitles:@"I want to", nil];
    alert.tag = 3;
    [alert show];
}

-(void) refreshAfterDelete
{
    [self updateData];
}

@end
