//
//  AddPhotoViewController.m
//  TechnicalTest
//
//  Created by David Velarde on 11/23/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "AddPhotoViewController.h"
#import "DataAccessRep.h"
@interface AddPhotoViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtDescription;
@property (weak, nonatomic) IBOutlet UITextField *txtUrl;

@end

@implementation AddPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)touchedDone:(id)sender {
    
    NSString *errorMessage = @"Oops! ";
    
    if([self.txtName.text length] <= 1)
    {
        errorMessage = [errorMessage stringByAppendingString:@"You must enter a valid name"];
    }
    else
    {
        if([self.txtDescription.text length] <= 1)
        {
            errorMessage = [errorMessage stringByAppendingString:@"You must enter a valid description"];
        }
        else
        {
            NSString *urlString = self.txtUrl.text;
            NSString *urlRegEx =
            @"(http|https)://([\\w-]+\\.)+[\\w-]+(/[\\w- ./]*)+\\.(?:gif|jpg|jpeg|png|bmp|GIF|JPEG|JPG|PNG|BMP|Gif|Jpg|Jpeg|Png|Bmp)$";
            NSPredicate *urlPredic = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
            
            BOOL isValidURL = [urlPredic evaluateWithObject:urlString];
            
            if(!isValidURL)
            {
                errorMessage = [errorMessage stringByAppendingString:@"You must enter a valid URL"];
            }
        }
    }
    
    if([errorMessage isEqualToString:@"Oops! "]){
        
        BOOL success = [DataAccessRep createPhotoWithName:self.txtName.text Description:self.txtDescription.text andURL:self.txtUrl.text];
        if(success)
        {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"We apologize but we can not register the photo at this time. But we are working on it." delegate:nil cancelButtonTitle:@"Got it" otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:errorMessage delegate:nil cancelButtonTitle:@"Got it" otherButtonTitles:nil];
        [alert show];
    }
    
    
}

@end
