//
//  ViewPhotoViewController.m
//  TechnicalTest
//
//  Created by David Velarde on 11/23/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "ViewPhotoViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "NetworkingRep.h"
#import "DataAccessRep.h"
#import "Photo.h"
#import "FileManager.h"
@interface ViewPhotoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;

@property (strong, nonatomic) Photo *objPhoto;
@property (weak, nonatomic) IBOutlet UITextView *lblDescription;


@end

@implementation ViewPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *objectId = [[NSUserDefaults standardUserDefaults] objectForKey:@"Photo"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"Photo"];
    self.objPhoto = [DataAccessRep retrievePhotoWithObjectId:objectId];
    
    [self.imgPhoto setImage:[FileManager getImageForID:objectId]];
    [self.navigationItem setTitle:[self.objPhoto name]];
    [self.lblDescription setText:[self.objPhoto desc]];
    [self.lblDescription setTextColor:[UIColor whiteColor]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)touchedDelete:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"You sure you want do delete this one?" delegate:self cancelButtonTitle:@"No!" otherButtonTitles:@"I do!", nil];
    alert.tag = 1;
    [alert show];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1){
        AFHTTPRequestOperationManager *objManager = [NetworkingRep getCustomizedManager];
        
        [objManager DELETE:[NSString stringWithFormat:@"https://api.parse.com/1/classes/Gallery/%@",self.objPhoto.objectId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [DataAccessRep deletePhoto:self.objPhoto];
            [self.navigationController popToRootViewControllerAnimated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshAfterDelete" object:nil];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
        
        
        
    }
}
@end
