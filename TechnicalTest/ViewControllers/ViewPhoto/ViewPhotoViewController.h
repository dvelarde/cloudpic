//
//  ViewPhotoViewController.h
//  TechnicalTest
//
//  Created by David Velarde on 11/23/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewPhotoViewController : UIViewController<UIAlertViewDelegate>

@end
