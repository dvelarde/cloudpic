//
//  FinishViewController.m
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "FinishViewController.h"
#import "DataAccessRep.h"

@interface FinishViewController ()

@end

@implementation FinishViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    User *objUser = [DataAccessRep retrieveUser];
    self.lblWelcomeMessage.text = [NSString stringWithFormat:@"Welcome then %@ %@ it will be a pleasure for us the record every image you want and have it stored here.\n\nEverytime you need them just open this app and they will be ready for you.",objUser.firstname, objUser.lastname];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)touchedFinish:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
