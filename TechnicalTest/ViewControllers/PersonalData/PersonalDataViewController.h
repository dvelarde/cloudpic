//
//  PersonalDataViewController.h
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonalDataViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;

@end
