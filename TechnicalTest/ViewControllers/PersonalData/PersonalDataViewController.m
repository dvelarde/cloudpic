//
//  PersonalDataViewController.m
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "PersonalDataViewController.h"
#import "DataAccessRep.h"
@interface PersonalDataViewController ()

@end

@implementation PersonalDataViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)touchedContinue:(id)sender {
    
    NSString *errorMessage = @"Oops! ";
    
    if([self.txtFirstName.text length] <= 1)
    {
        errorMessage = [errorMessage stringByAppendingString:@"You must enter a valid first name"];
    }
    else
    {
        if([self.txtLastName.text length] <= 1)
        {
            errorMessage = [errorMessage stringByAppendingString:@"You must enter a valid last name"];
        }
    }
    
    if([errorMessage isEqualToString:@"Oops! "]){
        
        BOOL success = [DataAccessRep createUserWithFirstName:self.txtFirstName.text andLastName:self.txtLastName.text];
        if(success)
            [self performSegueWithIdentifier:@"Continue" sender:nil];
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"We apologize but we can not register you at this time. But we are working on it." delegate:nil cancelButtonTitle:@"Got it" otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:errorMessage delegate:nil cancelButtonTitle:@"Got it" otherButtonTitles:nil];
        [alert show];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
