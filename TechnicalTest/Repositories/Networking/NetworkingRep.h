//
//  NetworkingRep.h
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
@interface NetworkingRep : NSObject
+(AFHTTPRequestOperationManager *)getCustomizedManager;
@end
