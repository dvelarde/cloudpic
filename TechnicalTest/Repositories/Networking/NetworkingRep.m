//
//  NetworkingRep.m
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "NetworkingRep.h"
@implementation NetworkingRep

+(AFHTTPRequestOperationManager *)getCustomizedManager
{
    AFHTTPRequestOperationManager *objManager = [AFHTTPRequestOperationManager manager];
    [objManager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [objManager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [[objManager requestSerializer] setValue:@"GwgggZlpRytZ9aWLNweqLqku42AmtRDnxANwgMAL" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [[objManager requestSerializer] setValue:@"O7cemBDf9ljX5heCJsI3lKSpbZ9bxoFIaNJtStvf" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    return objManager;
}

@end
