//
//  FileManager.m
//  TechnicalTest
//
//  Created by David Velarde on 11/23/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "FileManager.h"

@implementation FileManager

+(void)saveImage:(UIImage *)image forID:(NSString *)ID
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = YES;
    BOOL exists = [fileManager fileExistsAtPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ImagesData"] isDirectory:&isDirectory];
    if(!exists || !isDirectory)
    {
        NSFileManager *fileManager = [NSFileManager defaultManager];
        [fileManager createDirectoryAtPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ImagesData"] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSData *dataAEscribir = UIImageJPEGRepresentation(image, 1.0F);
    
    NSString *strDestino = [NSString stringWithFormat:@"%@/%@.jpg",[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ImagesData"],ID];
    
    [dataAEscribir writeToFile:strDestino atomically:YES];
    
}
+(UIImage *)getImageForID:(NSString *)ID
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = YES;
    BOOL exists = [fileManager fileExistsAtPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ImagesData"] isDirectory:&isDirectory];
    if(!exists || !isDirectory)
        return [UIImage imageNamed:@"placeholderImage"];
    else{
        
        NSString *strFuente = [NSString stringWithFormat:@"%@/%@.jpg",[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ImagesData"],ID];
        
        BOOL exists2 = [fileManager fileExistsAtPath:strFuente isDirectory:&isDirectory];
        
        if(!exists2 || isDirectory)
            return [UIImage imageNamed:@"placeholderImage"];
        
        return [UIImage imageWithContentsOfFile:strFuente];
    }
}

+(BOOL)imageExistsForID:(NSString *)ID
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = YES;
    BOOL exists = [fileManager fileExistsAtPath:[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ImagesData"] isDirectory:&isDirectory];
    if(!exists || !isDirectory)
        return NO;
    else{
        NSString *strFuente = [NSString stringWithFormat:@"%@/%@.jpg",[NSHomeDirectory() stringByAppendingPathComponent:@"Documents/ImagesData"],ID];
        
        BOOL exists2 = [fileManager fileExistsAtPath:strFuente isDirectory:&isDirectory];
        
        if(!exists2 || isDirectory)
            return NO;
        
        return YES;
    }
}
@end
