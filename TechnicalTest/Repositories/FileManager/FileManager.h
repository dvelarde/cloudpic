//
//  FileManager.h
//  TechnicalTest
//
//  Created by David Velarde on 11/23/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FileManager : NSObject

+(void)saveImage:(UIImage *)image forID:(NSString *)ID;
+(UIImage *)getImageForID:(NSString *)ID;
+(BOOL)imageExistsForID:(NSString *)ID;
@end
