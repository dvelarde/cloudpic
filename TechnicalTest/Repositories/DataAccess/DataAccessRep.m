//
//  DataAccessRep.m
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "DataAccessRep.h"
#import "AppDelegate.h"


@implementation DataAccessRep

+(User *) retrieveUser
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User"
                                              inManagedObjectContext:objContext];
    
    [fetchRequest setEntity:entity];

    NSError *error;
    
    NSArray *fetchedObjects = [objContext executeFetchRequest:fetchRequest error:&error];
    
    if(error)
        return nil;
    else
    {
        if([fetchedObjects count] > 0)
        {
            return (User *)[fetchedObjects objectAtIndex:0];
        }
        else
            return nil;
    }
}
+(BOOL) deleteUser:(User *)objUser
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    [objContext deleteObject:objUser];
    
    NSError *error;
    
    if (![objContext save:&error])
        return NO;
    else
        return YES;
    
}
+(BOOL) createUserWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName
{
    User *existingUser = [self retrieveUser];
    if(existingUser != nil)
    {
        [self deleteUser:existingUser];
    }
    
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
        User *objUser = [NSEntityDescription
                                          insertNewObjectForEntityForName:@"User"
                                          inManagedObjectContext:objContext];
        objUser.firstname = firstName;
        objUser.lastname = lastName;
    
        NSError *error;
        if (![objContext save:&error])
            return NO;
        else
            return YES;
    
}

+(NSArray *)retrievePhotos
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo"
                                              inManagedObjectContext:objContext];
    
    [fetchRequest setEntity:entity];
    
    NSError *error;
    
    NSArray *fetchedObjects = [objContext executeFetchRequest:fetchRequest error:&error];
    
    if(error || fetchedObjects == nil || [fetchedObjects count] == 0)
        return nil;
    else
        return fetchedObjects;
}

+(BOOL)createPhotoFromDictionary:(NSDictionary *)objPhoto
{
    NSString *objectId = [objPhoto objectForKey:@"objectId"];
    NSString *url = [objPhoto objectForKey:@"pictureUrl"];
    NSString *name = [objPhoto objectForKey:@"name"];
    NSString *desc = [objPhoto objectForKey:@"description"];
    
    NSString *dateCreated = [objPhoto objectForKey:@"createdAt"];
    NSString *dateUpdated = [objPhoto objectForKey:@"updatedAt"];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH-mm-ss.SSS'Z'"];
    
    NSDate *dateCreatedValue = [dateFormatter dateFromString:dateCreated];
    NSDate *dateUpdatedValue = [dateFormatter dateFromString:dateUpdated];
    
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];

    Photo *objPhotoModel = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Photo"
                     inManagedObjectContext:objContext];
    objPhotoModel.objectId = objectId;
    objPhotoModel.name = name;
    objPhotoModel.desc = desc;
    objPhotoModel.url = url;
    objPhotoModel.dateCreated = dateCreatedValue;
    objPhotoModel.dateModified = dateUpdatedValue;
    objPhotoModel.uploaded = [NSNumber numberWithBool:YES];
    objPhotoModel.ableToUpdate = [NSNumber numberWithBool:YES];
    NSError *error;
    if (![objContext save:&error])
        return NO;
    else
        return YES;
}

+(BOOL)deletePhoto:(Photo *)objPhoto
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    [objContext deleteObject:objPhoto];
    
    NSError *error;
    
    if (![objContext save:&error])
        return NO;
    else
        return YES;
}

+(Photo *)retrievePhotoWithURL:(NSString *)url
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo"
                                              inManagedObjectContext:objContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"url == %@", url];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    
    NSArray *fetchedObjects = [objContext executeFetchRequest:fetchRequest error:&error];
    
    if(error)
        return nil;
    else{
        if(fetchedObjects != nil && [fetchedObjects count] > 0)
            return [fetchedObjects objectAtIndex:0];
        else
            return nil;
    }
    
    
}
+(Photo *)retrievePhotoWithObjectId:(NSString *)objectId
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo"
                                              inManagedObjectContext:objContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"objectId == %@", objectId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    
    NSArray *fetchedObjects = [objContext executeFetchRequest:fetchRequest error:&error];
    
    if(error)
        return nil;
    else{
        if(fetchedObjects != nil && [fetchedObjects count] > 0)
            return [fetchedObjects objectAtIndex:0];
        else
            return nil;
    }
    
    
}
+(BOOL) updatePhoto:(NSDictionary *)objPhoto
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo"
                                              inManagedObjectContext:objContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateCreated == %@", [objPhoto objectForKey:@"dateCreated"]];
    
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    
    NSArray *fetchedObjects = [objContext executeFetchRequest:fetchRequest error:&error];
    
    if(error)
        return NO;
    else{
        if(fetchedObjects != nil && [fetchedObjects count] > 0)
        {
            Photo *currentPhoto = [fetchedObjects objectAtIndex:0];
            currentPhoto.name = [objPhoto objectForKey:@"name"];
            currentPhoto.desc = [objPhoto objectForKey:@"description"];
            currentPhoto.url = [objPhoto objectForKey:@"pictureUrl"];
            currentPhoto.ableToUpdate = [objPhoto objectForKey:@"ableToUpdate"] != nil ? [objPhoto objectForKey:@"ableToUpdate"] : [NSNumber numberWithBool:YES];
            currentPhoto.uploaded = [objPhoto objectForKey:@"uploaded"] != nil ? [objPhoto objectForKey:@"uploaded"] : [NSNumber numberWithBool:YES];
            
            if (![objContext save:&error])
                return NO;
            else
                return YES;
        }
        else
            return YES;
    }
}
+(BOOL) hasThisPhotoChangedOvertime:(NSDictionary *)objPhoto
{
    NSString *url = [objPhoto objectForKey:@"pictureUrl"];
    Photo *currentPhoto = [self retrievePhotoWithURL:url];
    if(currentPhoto == nil)
    {
        [self createPhotoFromDictionary:objPhoto];
        return NO;
    }
    
    NSString *dateUpdated = [objPhoto objectForKey:@"updatedAt"];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH-mm-ss.SSS'Z'"];
    
    NSDate *dateUpdatedValue = [dateFormatter dateFromString:dateUpdated];
    
    if([dateUpdatedValue timeIntervalSinceDate:currentPhoto.dateModified] > 0)
        return YES;
    else
        return NO;
}

+(BOOL) createPhotoWithName:(NSString *)name Description:(NSString *)desc andURL:(NSString *)url
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    Photo *objPhotoModel = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Photo"
                            inManagedObjectContext:objContext];
    objPhotoModel.objectId = @"";
    objPhotoModel.name = name;
    objPhotoModel.desc = desc;
    objPhotoModel.url = url;
    objPhotoModel.dateCreated = [NSDate date];
    objPhotoModel.dateModified = [NSDate date];
    objPhotoModel.uploaded = [NSNumber numberWithBool:NO];
    objPhotoModel.ableToUpdate = [NSNumber numberWithBool:NO];
    NSError *error;
    if (![objContext save:&error])
        return NO;
    else
        return YES;
}
+(NSDictionary *) dictionaryFromPhoto:(Photo *)objPhoto
{
    NSMutableDictionary *objDict = [NSMutableDictionary new];
    
    @try{
        [objDict setObject:objPhoto.objectId forKey:@"objectId"];
        [objDict setObject:objPhoto.name forKey:@"name"];
        [objDict setObject:objPhoto.desc forKey:@"description"];
        [objDict setObject:objPhoto.url forKey:@"pictureUrl"];
        [objDict setObject:objPhoto.dateCreated forKey:@"dateCreated"];
        [objDict setObject:objPhoto.dateModified forKey:@"dateModified"];
    }
    @catch(NSException *ex)
    {
        return nil;
    }
    return objDict;
}

+(NSArray *) getDataToUpload
{
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo"
                                              inManagedObjectContext:objContext];
    [fetchRequest setEntity:entity];

    NSPredicate *predicateName = [NSPredicate predicateWithFormat:@"uploaded == %@", [NSNumber numberWithBool:NO]];
    NSPredicate *predicateSSID = [NSPredicate predicateWithFormat:@"ableToUpdate == %@", [NSNumber numberWithBool:YES]];
    
    NSArray *subPredicates = [NSArray arrayWithObjects:predicateName, predicateSSID, nil];
    
    NSPredicate *andPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    
    [fetchRequest setPredicate:andPredicate];
    
    NSError *error;
    
    NSArray *fetchedObjects = [objContext executeFetchRequest:fetchRequest error:&error];
    
    NSMutableArray *dataToUpdate = [NSMutableArray new];
    
    if(error)
        return nil;
    else{
        if(fetchedObjects != nil || [fetchedObjects count] > 0)
        {
            for(Photo *objPhoto in fetchedObjects)
            {
                NSMutableDictionary *objDict = [NSMutableDictionary new];
                [objDict setObject:objPhoto.name forKey:@"name"];
                [objDict setObject:objPhoto.desc forKey:@"description"];
                [objDict setObject:objPhoto.url forKey:@"pictureUrl"];
                [dataToUpdate addObject:objDict];
            }
        }
        else
            return nil;
    }
    return dataToUpdate;
}

+(BOOL) updateDatabaseWithRegisteredData:(NSDictionary *)response ForUrl:(NSString *)url
{
    NSString *createdAt = [response objectForKey:@"createdAt"];
    NSString *objectId = [response objectForKey:@"objectId"];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH-mm-ss.SSS'Z'"];
    
    AppDelegate *objDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *objContext = [objDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Photo"
                                              inManagedObjectContext:objContext];
    [fetchRequest setEntity:entity];
    NSPredicate *predicateUrl = [NSPredicate predicateWithFormat:@"url == %@", url];
    NSPredicate *predicateUploaded = [NSPredicate predicateWithFormat:@"uploaded == %@", [NSNumber numberWithBool:NO]];
    NSPredicate *predicateAble = [NSPredicate predicateWithFormat:@"ableToUpdate == %@", [NSNumber numberWithBool:YES]];
    NSPredicate *predicateObjectId = [NSPredicate predicateWithFormat:@"objectId == %@", @""];
    
    NSArray *subPredicates = [NSArray arrayWithObjects:predicateUrl, predicateAble, predicateUploaded, predicateObjectId, nil];
    
    NSPredicate *andPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:subPredicates];
    
    [fetchRequest setPredicate:andPredicate];
    
    NSError *error;
    
    NSArray *fetchedObjects = [objContext executeFetchRequest:fetchRequest error:&error];
    
    if(error)
        return NO;
    else{
        if(fetchedObjects != nil && [fetchedObjects count] > 0)
        {
            Photo *currentPhoto = [fetchedObjects objectAtIndex:0];
            
            currentPhoto.dateCreated = [dateFormatter dateFromString:createdAt];
            currentPhoto.uploaded = [NSNumber numberWithBool:YES];
            currentPhoto.objectId = objectId;
            
            if (![objContext save:&error])
                return NO;
            else
                return YES;
        }
        else
            return YES;
    }
    
}

@end
