//
//  DataAccessRep.h
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Photo.h"

@interface DataAccessRep : NSObject

//User
+(User *) retrieveUser;
+(BOOL) createUserWithFirstName:(NSString *)firstName andLastName:(NSString *)lastName;

//Photos
+(NSArray *)retrievePhotos;
+(BOOL)createPhotoFromDictionary:(NSDictionary *)objPhoto;
+(BOOL)deletePhoto:(Photo *)objPhoto;
+(Photo *)retrievePhotoWithURL:(NSString *)url;
+(Photo *)retrievePhotoWithObjectId:(NSString *)objectId;
+(BOOL) updatePhoto:(NSDictionary *)objPhoto;
+(BOOL) hasThisPhotoChangedOvertime:(NSDictionary *)objPhoto;
+(BOOL) createPhotoWithName:(NSString *)name Description:(NSString *)desc andURL:(NSString *)url;
+(NSDictionary *) dictionaryFromPhoto:(Photo *)objPhoto;
+(NSArray *) getDataToUpload;
+(BOOL) updateDatabaseWithRegisteredData:(NSDictionary *)response ForUrl:(NSString *)url;
@end
