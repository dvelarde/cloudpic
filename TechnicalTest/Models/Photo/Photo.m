//
//  Photo.m
//  TechnicalTest
//
//  Created by David Velarde on 11/23/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "Photo.h"


@implementation Photo

@dynamic dateCreated;
@dynamic dateModified;
@dynamic desc;
@dynamic name;
@dynamic url;
@dynamic objectId;
@dynamic uploaded;
@dynamic ableToUpdate;

@end
