//
//  Photo.h
//  TechnicalTest
//
//  Created by David Velarde on 11/23/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Photo : NSManagedObject

@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSDate * dateModified;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSNumber * uploaded;
@property (nonatomic, retain) NSNumber * ableToUpdate;

@end
