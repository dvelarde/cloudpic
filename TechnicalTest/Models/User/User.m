//
//  User.m
//  TechnicalTest
//
//  Created by David Velarde on 11/22/14.
//  Copyright (c) 2014 David Velarde. All rights reserved.
//

#import "User.h"


@implementation User

@dynamic firstname;
@dynamic lastname;

@end
